import request from "supertest";
import Moralis from "moralis/node.js";
import { expect } from "chai";
import app from "../app.js";

const ethers = Moralis.web3Library;
const wallet = new ethers.Wallet(
  "d6e9e8bd736ccfebddd8f19a97a50ec85131bdf4c254862ff1515602f65b83f1"
);
const server = request(app);

const now = Date.now();
const nowPlus24Hours = now + 24 * 60 * 60 * 1000;
const nowPlus12Hours = now + 12 * 60 * 60 * 1000;
const nowPlus10Days = now + 10 * 24 * 60 * 60 * 1000;
const nowPlus15Days = now + 15 * 24 * 60 * 60 * 1000;
const nowPlus40Days = now + 40 * 24 * 60 * 60 * 1000;

const createData = async (start, end, item_type = "land") => {
  const message = JSON.stringify({
    start: start,
    end: end,
    item_type: item_type,
    plot_int: 1111,
    sale_type: "sale",
    pricing_model: "auction",
    seller: wallet.address,
  });
  const hash = await wallet.signMessage(message);

  return {
    msg: message,
    hash: hash,
  };
};
console.log(wallet.address.toLowerCase());
//DATES
describe("* SALE CREATION SERVICE - /api/sales/create", () => {
  describe("* End date before start date", () => {
    let response;
    before(async () => {
      const data = await createData(nowPlus24Hours, now);
      response = await server.post("/api/sales/create").send(data);
    });
    it("Returns 400", () => {
      expect(response.status).to.be.equal(400);
    });
    it("Contains error message", () => {
      expect(response.body).to.be.equal('"start" must be less than "ref:end"');
    });
  });

  describe("* Start days is more than 7 days from now", () => {
    let response;
    before(async () => {
      const data = await createData(nowPlus10Days, nowPlus15Days);
      response = await server.post("/api/sales/create").send(data);
    });
    it("Returns 400", () => {
      expect(response.status).to.be.equal(400);
    });
    it("Contains error message", () => {
      expect(response.body).to.be.contain(
        "start must be within 7 days from now"
      );
    });
  });

  describe("* End date is less than 24 hrs", () => {
    let response;
    before(async () => {
      const data = await createData(now, nowPlus12Hours);
      response = await server.post("/api/sales/create").send(data);
    });
    it("Returns 400", () => {
      expect(response.status).to.be.equal(400);
    });
    it("Contains error message", () => {
      expect(response.body).to.be.contain(
        "end must be between start + 24hrs and start + 30 days"
      );
    });
  });
  describe("* End date is more than 30 days", () => {
    let response;
    before(async () => {
      const data = await createData(now, nowPlus40Days);
      response = await server.post("/api/sales/create").send(data);
    });
    it("Returns 400", () => {
      expect(response.status).to.be.equal(400);
    });
    it("Contains error message", () => {
      expect(response.body).to.contain(
        "end must be between start + 24hrs and start + 30 days"
      );
    });
  });

  describe("* Item Type is not land or equipment", function (done) {
    let response;
    before(async () => {
      const data = await createData(now, nowPlus15Days, "badland");
      response = await server.post("/api/sales/create").send(data);
    });
    it("Returns 400 ", async () => {
      expect(response.status).to.be.equal(400);
    });
    it("Contains error message", () => {
      expect(response.body).to.contain(
        '"item_type" must be one of [land, equipment]'
      );
    });
  });
});
