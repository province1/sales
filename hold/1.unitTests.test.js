import { expect } from "chai";
import { FindBidsBySaleId,FindLandByPlotInt } from "../utils/db_service.js";
import {
  IsSellerOwner,
  isUsersHighestBid,
  getLeadingBid,
  isHigherThanLeadingBid,
  hideBidData,
  getBidsByUserAddress,
} from "../utils/helpers.js";

const addressWithLand = "0xe46ae06141e46651e9f45172bfdb981445103095";
const addressWithoutLand = "0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b";
const plotsOwned = [3];

describe("* UNIT TESTING", function () {

  describe("* IsSellerOwner", () => {
    it("If seller owns a plot doesnt throw an error", async () => {
      const landArray = await FindLandByPlotInt(plotsOwned[0]);
      const testAllPlots = () =>
        plotsOwned.forEach((plot) => IsSellerOwner(landArray,addressWithLand));
      expect(testAllPlots).to.not.throw();
    });

    it("If seller doesnt own a plot throws an error", async () => {
      const landArray = await FindLandByPlotInt(1);
      const testAllPlots = () =>
        plotsOwned.forEach((plot) => IsSellerOwner(landArray,addressWithoutLand));
      expect(testAllPlots).to.throw();
    });
  });

  describe("* isUsersHighestBid", () => {
    const userBids = [{ bid: 10 }, { bid: 1 }, { bid: 2 }, { bid: 6 }];
    const newBidHigher = { bid: 11 };
    const newBidLower = { bid: 5 };

    it("Bid higher than current bid doesn't throw an error", () => {
      expect(() => isUsersHighestBid(userBids, newBidHigher)).to.not.throw();
    });

    it("Bid lower than current bid throws an error", async () => {
      expect(() => isUsersHighestBid(userBids, newBidLower)).to.throw();
    });
  });

  describe("* getLeadingBid", () => {
    const oneBid = [{ bid: 10 }];
    const bids = [
      { bid: 10 },
      { bid: 1 },
      { bid: 2 },
      { bid: 6 },
      { bid: 50 },
      { bid: -2 },
      { bid: 0 },
    ];

    it("For an array of bids, the function should return the highest bid.", () => {
      expect(getLeadingBid(bids)).to.equal(11);
      expect(getLeadingBid(oneBid)).to.equal(11);
    });
  });

  describe("* IsHigherThanLeadingBid", function () {
    const oneBid = { bid: 1000000 };
    const leadgingBid = 100;
  
    it("Submit a bid of 1000000 when the leading bid is 100. Should not throw an error.", () => {
      expect(() => isHigherThanLeadingBid(leadgingBid, oneBid)).to.not.throw();
    });
  });

  describe("* HideBidData", function () {
    const date = Date.now();
    const bidArray = [
      {
        _id: 1,
        address: "0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b",
        date: date,
        bid: 100,
        propertyX: "random things",
        notAProperty: "xxx",
      },
      {
        _id: 2,
        address: "0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b",
        date: date,
        bid: 100,
        propertyX: "random things",
      },
      {
        _id: 3,
        address: "0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b",
        date: date,
        bid: 1000,
        propertyX: "random things2232",
      },
    ];
  
    it("Submit bids, verfiy that bidding data is not returned.", () => {
      const hiddenArray = hideBidData(bidArray);
      hiddenArray.forEach((item) => {
        expect(item).to.not.have.all.keys("propertyX", "notAProperty", "bid");
        expect(item).to.have.all.keys("_id", "address", "date");
      });
    });
  });

  describe("* getBidsByUserAddress", function () {
    it("Input bidsOnSale array, verify that each address submitted has the correct corresponding bid", async () => {
      const bidsOnSale = await FindBidsBySaleId(
        "5482d471-3b4c-469a-ac04-b5b42db1b07b"
      );
      const userBids = getBidsByUserAddress(bidsOnSale, addressWithoutLand);
      userBids.forEach((bid) => {
        expect(bid).to.include.keys("bid");
        expect(bid.address).to.equal(addressWithoutLand);
        expect(bid.address).to.not.equal(addressWithLand);
      });
    });
  });
  
});
