import * as chai from 'chai';
var should = chai.should();

import { FindLandByPlotInt, GetAllSales, GetAllCreatedAuctions, FindBidsBySaleId, FindEquipmentBySerial, FindSaleById, NetworkRequest, GetSalesByPlotInt, GetSalesBySerial} from "../utils/db_read.js";
import { test_data } from './test_request_data.js'


class sim_axios{

    plot_query = async(request) => {
        return test_data[request.url][request.data.plot_int]
    }
    
    sales_all_query = async(request, idx) => {
        return test_data[request.url][idx]
    }
    
    sales_id_query = async(request) => {
        return test_data[request.url][request.data.query.sale_id];
    }
    sales_plot_query = async(request) => {
        return test_data[request.url]["plot_int"][request.data.query.plot_int];
    }
    
    bids_saleid_query = async(request, idx) => {
        return test_data[request.url][idx]
    }
    equipment_serial_query = async(request) => {
        return test_data[request.url][request.data.query.serial]
    }
    
}

describe("Check FindLandByPlotInt", () => {

    it("Should return a known land plot mockup (db integration not tested here)", async () => {
        
        const db = new sim_axios()
        const network_request = new NetworkRequest("db_service", db.plot_query.bind(db) );
        const landArray = await network_request.plot_query(12);
        landArray[0]._id.should.be.equal('62ab14524d32769954701a51')
        landArray.length.should.be.equal(1)
    });
    
    
    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should connect to the database and return a result for a known plot of land", async () => {
        const landArray = await FindLandByPlotInt(12)
        landArray.length.should.be.equal(1)
    });

    
});


describe("GetAllSales", () => {

    
    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return without an error and a result length of zero or more.", async () => {
        const sales = await GetAllSales();
        sales.error.should.equal(false)
        sales.result.length.should.be.equal(15)
    })
    
    it("Should return 1 matching sales_id for the simulated data", async () => {
        const db = new sim_axios()
        const network_request = new NetworkRequest("db_service",db.sales_all_query.bind(db));
        //const db = new sim_axios();
        const sales = await network_request.sales_query(0)
        sales.error.should.equal(false)
        sales.result[0].sale_id.should.be.equal("916ae33b-77e6-4ba2-99c7-fdb2242b1b4d")
    })

});


describe("GetAllCreatedAuctions", () => {

    it("Should return 1 matching sales_id for the simulated data", async () => {
        const db = new sim_axios();
        const network_request = new NetworkRequest("db_service", db.sales_all_query.bind(db));
        const auctions = await network_request.auction_query(1)
         auctions.length.should.be.equal(5)
         auctions[0].sale_id.should.be.equal('d9d8c3b2-f3f9-4c26-88eb-332033d72538')
    })

    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return without an error and a result length of zero or more.", async () => {
        const auctions = await GetAllCreatedAuctions();
        auctions.length.should.be.equal(5)
    })

});


describe("FindBidsBySaleId", () => {

    it("Should return without an error and a result length of zero or more.", async () => {
        const db = new sim_axios();
        const network_request = new NetworkRequest("db_service", db.bids_saleid_query.bind(db));
        const auctions = await network_request.bids_query("8cdc4228-30c6-47fa-ab33-902e3b3c4553", 0)
        
        const total = auctions.reduce( (x,y) => { 
            return x + y.bid }, 0
            )
        
        total.should.be.equal(520);
        auctions.length.should.be.equal(4);
    })


    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return without an error and a result length of zero or more.", async () => {
        const sale = await FindBidsBySaleId('8cdc4228-30c6-47fa-ab33-902e3b3c4553');
        sale.length.should.be.equal(4)
    })

});



describe("FindEquipmentBySerial", () => {

    it("Should ", async () => {
        const db = new sim_axios()
        const network_request = new NetworkRequest("db_service", db.equipment_serial_query.bind(db));
        const equipment = await network_request.equipment_serial_query(0)
        equipment[0].owner.should.be.equal("0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a")
        equipment.length.should.be.equal(1)
    })

    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return without an error and a result length of zero or more.", async () => {
        const equipment = await FindEquipmentBySerial(0);
        equipment.length.should.be.equal(1)
    })

});

describe("FindSaleById", () => {

    it("Should return a known value for a sale query using simulated data", async () => {
       const db = new sim_axios()
       const network_request = new NetworkRequest("db_service", db.sales_id_query.bind(db));
       const sale = await network_request.find_factory({sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72537'})
        sale.plot_int.should.be.equal(1)
    })
    

    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return without an error and a result length of zero or more.", async () => {
        const sale = await FindSaleById({sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72537'});
        sale.plot_int.should.be.equal(2)
    })
    
    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return an empty array for a bad sales ID", async () => {
        const sale = await FindSaleById({sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d7253'});
        //console.log(sale)
        sale.length.should.be.equal(0)
    })

});

describe("FindFactory2", () => {


    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should ", async () => {
        const sale = await GetSalesByPlotInt({plot_int: 1});
        sale[0].plot_int.should.be.equal(1)
        sale.length.should.be.equal(1)
    })
    
    
    it("Should return known sales data when querying by plot using simulated data ", async () => {
        const db = new sim_axios()
        const network_request = new NetworkRequest("db_service", db.sales_plot_query.bind(db));
        const sale = await network_request.find_factory2({plot_int: 1})
        sale[0].plot_int.should.be.equal(1)
        sale[0].seller.should.be.equal("0x5a208fae7d74b521498ca29036b0f810ec411571")
        sale.length.should.be.equal(1)
    })
    
    
});

describe("GetSalesBySerial", () => {


    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should ", async () => {
        const sale = await GetSalesBySerial({serial: 1})
        console.log(sale)
        //sale[0].plot_int.should.be.equal(1)
        //sale.length.should.be.equal(1)
    })
    
    /*
    it("Should return known sales data when querying by plot using simulated data ", async () => {
        const db = new sim_axios()
        const network_request = new NetworkRequest("db_service", db.sales_plot_query.bind(db));
        const sale = await network_request.find_factory2({plot_int: 1})
        sale[0].plot_int.should.be.equal(1)
        sale[0].seller.should.be.equal("0x5a208fae7d74b521498ca29036b0f810ec411571")
        sale.length.should.be.equal(1)
    })
    */
    
    
});