import * as chai from 'chai';
var should = chai.should();
import { expect } from "chai";

import { strict as assert } from 'node:assert';

import {IsSellerOwner, IsIdle, IsOnSale} from '../utils/helpers.js'
import {SellerIsNotOwnerError, EquipmentIsNotIdleError, ItemAlreadyOnSale} from '../utils/errors.js'

import { test_data } from './test_request_data.js'


const data = test_data['equipment'][0]

const result = IsSellerOwner(data, '0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a')

const bad_address = "0xb8a3451a72da6f71a2afca4bb4ad451855c44a9b"
const owner_address = "0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a"




describe("IsSellerOwner", () => {

    
    it("It should return an error when the seller does not match the listed owner for a simulated piece of equipment", () => {

        assert.throws( () => {
            IsSellerOwner(data, bad_address)
            
        }, (err) => {
 
          assert.strictEqual(err,  SellerIsNotOwnerError)  
          return true
            
        } )
        
    });
    
    it("It should return an error when the seller does not match the listed owner for a simulated piece of equipment", () => {

        
        const test_fn = ()=>{ IsSellerOwner(data, owner_address)};
        expect(test_fn).to.not.throw();
      
        
    });
    
    
    // ToDo: More explicit check might be needed to throw an error if there is no matching equipment number for the given serial number
    it("It should return an error when an empty array is submitted - simulating that the serial number does not exist.", () => {


        assert.throws( () => {
            IsSellerOwner([], bad_address)
            
        }, (err) => {
            
          assert.strictEqual(err.name,  "TypeError")  
          assert.strictEqual(err.message,  "Cannot read properties of undefined (reading 'owner')")  
          return true
            
        } )
        
        
      
        
    });
    
    
});


describe("isIdle", () => {

    
    it("It should not throw an error when the equipment is idle", () => {

        const test_fn = ()=>{ IsIdle(data)};
        expect(test_fn).to.not.throw();
            
    });
    
    it("It should throw an error unless the equipment status is idle", () => {
        
        var test_data = data;
        test_data[0].status = "deployed"
        
        assert.throws( () => {
            IsIdle(test_data)
            
        }, (err) => {
            
          assert.strictEqual(err,  EquipmentIsNotIdleError)  
          return true
            
        } )
            
    });
    

    
    
});




describe("IsOnSale", () => {

    
    it("It should return an error when the seller does not match the listed owner for a simulated piece of equipment", () => {

        const test_fn = ()=>{ IsOnSale(data)};
        expect(test_fn).to.not.throw();
            
    });
    
    
    it("It should return an error when the seller does not match the listed owner for a simulated piece of equipment", () => {
        
        var test_data = data;
        test_data[0].status = "deployed"
        
        assert.throws( () => {
            IsIdle(test_data)
            
        }, (err) => {
            
          assert.strictEqual(err,  EquipmentIsNotIdleError)  
          return true
            
        } )
            
    });
    
    

    
    
});