import request from "supertest";
import Moralis from "moralis/node.js";
import { expect } from "chai";
import app from "../app.js";

const ethers = Moralis.web3Library;
const wallet = new ethers.Wallet("d6e9e8bd736ccfebddd8f19a97a50ec85131bdf4c254862ff1515602f65b83f1");
const server = request(app);

const fixedPriceID = "e1d7513b-17c6-44f5-acbb-d273b13eedde";
const endedSaleID = "fixed-price-ended-sale-id";
const notEndedSaleID = "fixed-price-not-ended-sale-id";
const auctionID = "41967f23-80f7-492d-be0c-e0faba741f82";
const unexistingSaleID = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

const createData = async( sale_id, bid ) => {
  const message = JSON.stringify({
    bid: bid || 100,
    address: wallet.address,
    timestamp:Date.now(),
    sale_id: sale_id
  });
  const hash = await wallet.signMessage(message);

  return {
    msg: message,
    hash: hash,
  };
};

describe("* CANCELLATION SERVICE - /api/sales/cancel", (done) => {

  describe("* Sale ID does not exist",()=>{
    let response
    before(async()=>{
      const data = await createData(unexistingSaleID);
      response = await server.post("/api/sales/cancel").send(data);
    })
    it("Returns 400", ()=>{
      expect(response.status).to.equal(400);
    })
    it("Contains error message", () => {
      expect(response.body.message).to.contain("Sale ID not found");
    });

    it("Contains error Code 12", () => {
      expect(response.body.error_code).to.equal(12);
    });
  })

  describe("* Sale ID is NOT fixed price", () => {
    let response;

    before(async () => {
      const data = await createData(auctionID)
      response = await server
        .post("/api/sales/cancel")
        .send(data);
    });

    it("Returns 400", () => {
      expect(response.status).to.equal(400);
    });

    it("Contains error message", () => {
      expect(response.body.message).to.contain("Sale is not fixed price");
    });

    it("Contains error Code 10", () => {
      expect(response.body.error_code).to.equal(10);
    });
  });

  describe("* User does not own the land", () => {
    let response;
    before(async () =>{
      const data = await createData(notEndedSaleID)
      response = await server.post("/api/sales/cancel").send(data)
    })

    it("Returns 400", () => {
      expect(response.status).to.equal(400);
    });

    it("Contains error message", () => {
      expect(response.body.message).to.contain(
        "Seller and Owner are not the same"
      );
    });

    it("Returns correct error code", () => {
      expect(response.body.error_code).to.equal(3);
    });
  });

  describe("* Sale has ended", ()=>{
    let response
    before(async ()=>{
      const data = await createData(endedSaleID)
      response = await server.post("/api/sales/cancel").send(data)
    })
    it("Returns 400",()=>{
      expect(response.status).to.equal(400)
    })
    it("Contains error message",()=>{
      expect(response.body.message).to.contain("Sale has ended")
    })
    it("Returns correct error code",()=>{
      expect(response.body.error_code).to.equal(9)
    })
  })
  describe("* JSON Message is not valid",()=>{
    let response
    before(async()=>{
      const data = await createData(fixedPriceID)
      const corruptedData = {...data, msg: "corrupted"}
      response = await server.post("/api/sales/cancel").send(corruptedData)
    })

    it("Returns 400",()=>{
      expect(response.status).to.equal(400)
    })
    it("Contains error message",()=>{
      expect(response.body).to.contain("Unexpected token")
    })
  })

  describe("* Signature is not valid",()=>{
    let response
    before(async()=>{
      const data = await createData(fixedPriceID)
      const corruptedData = {...data, hash: "corrupted"}
      response = await server.post("/api/sales/cancel").send(corruptedData)
    })

    it("Returns 400",()=>{
      expect(response.status).to.equal(400)
    })
    it("Contains error message",()=>{
      expect(response.body.message).to.contain("Signature is not valid")
    })
    it("Contains error code",()=>{
      expect(response.body.error_code).to.equal(2)
    })
  })
})
