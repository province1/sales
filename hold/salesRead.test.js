import request from 'supertest'
import {expect} from 'chai'
import app from '../app.js'
const server = request(app)

describe("* GET /api/sales/read", function(done){

    it("Returns Status 200", async () =>{
        const response = await server.get('/api/sales/read')
        expect(response.status).to.equal(200)

    })
    it("Body is an Array", async () =>{
        const response = await server.get('/api/sales/read')
        expect(response.body).to.be.an('array')
    })
})