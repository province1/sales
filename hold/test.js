import dotenv from "dotenv";
dotenv.config();

import * as chai from 'chai';
var should = chai.should();

import {API} from '../network/Api.js'

import {EquipmentNetworkRequest} from '../network/EquipmentNetwork.js'


describe("Equipment Network Request", () => {

    
    it("clear_lease(sale, buyer_address, days)", async () => {

        const Api = new API();
        const api_post = Api.post_request.bind(Api);
        const network_request = new EquipmentNetworkRequest(0,  api_post);
        const sale_data = {seller: '0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a', serial: 0}
        var result = await network_request.clear_lease(  sale_data  , "0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a", 180)
        result.error.should.equal(false)
        result.result.acknowledged.should.equal(true)
        
    })
})
