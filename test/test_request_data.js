export  let test_data = {
    
    "equipment": {
        
        0: 
            
            [
                {
                    _id: '628f8069990bbbd96ffd0b78',
                    serial: 0,
                    model: 'g1',
                    timestamp: 1653571689028,
                    owner: '0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a',
                    status: 'idle',
                    img_url: 'https://eep.province.gg/images/bulldozer.png',
                    description: 'description',
                    item: 'Bulldozer (G1)',
                    new_location: 1
                }
            ]

    },



    "/current_state": {
        0: {
            
            error: false,
            result: [
                {
                    _id: '628f8069990bbbd96ffd0b78',
                    serial: 0,
                    model: 'g1',
                    timestamp: 1653571689028,
                    owner: '0xb8a3451a72da6f71a2afca4bb4ad451855c44a9a',
                    status: 'idle',
                    img_url: 'https://eep.province.gg/images/bulldozer.png',
                    description: 'description',
                    item: 'Bulldozer (G1)',
                    new_location: 1
            }
            ]
        },
        
        'd9d8c3b2-f3f9-4c26-88eb-332033d72537': {
            
            error: false,
            result: [
               {
                _id: '629bb08661c541d116e2d7b2',
                timestamp: 1654239484944,
                seller: '0x5a208fae7d74b521498ca29036b0f810ec411571',
                plot_int: 1,
                start: 1654239477221,
                end: 1654325877221,
                item_type: 'land',
                sale_type: 'sale',
                pricing_model: 'auction',
                state: 'created',
                sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72537'
              },
            ]
        },
        
        plot_int:   {1: 
                        {
                            data:   {
                                error: false,
                                result: [
                                  {
                                    _id: '62a3655e527781a8153feebd',
                                    timestamp: 1654875523000,
                                    seller: '0x5a208fae7d74b521498ca29036b0f810ec411571',
                                    plot_int: 1,
                                    start: 1654239477221,
                                    end: 1654804957000,
                                    item_type: 'land',
                                    sale_type: 'sale',
                                    pricing_model: 'auction',
                                    state: 'ended',
                                    sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72531'
                                  }
                                ]
        
                            }
 
                        }
                    }

        },
    
    
    "/current_state_many": {
        
        0: 
            {error: false,
            result: [
                {
                    _id: '62ab07264d32769954701a4e',
                    start: 1655375953456,
                    end: 1655376060124,
                    item_type: 'land',
                    sale_type: 'sale',
                    seller: '0xb8b7c154398db6e61c8d8e8b5b97480dc625d490',
                    pricing_model: 'fixed-price',
                    timestamp: 1655376060192,
                    plot_int: 12,
                    price: 50,
                    state: 'ended',
                    sale_id: '916ae33b-77e6-4ba2-99c7-fdb2242b1b4d',
                    buyer: '0xf51ecd17f361847c6f1453a3d02fca16368dc7ea'
                },
                {
                  _id: '62a366c4527781a8153feebf',
                  timestamp: 1655372957000,
                  seller: '0x5a208fae7d74b521498ca29036b0f810ec411571',
                  plot_int: 2,
                  start: 1654239477221,
                  end: 1654804957000,
                  item_type: 'land',
                  sale_type: 'sale',
                  pricing_model: 'auction',
                  state: 'ended',
                  sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72538'
                }
            ]
        },
        
        1: {error: false,
            result:  [
              {
                _id: '629bba5961c541d116e2d7b5',
                timestamp: 1654372957000,
                seller: '0x5a208fae7d74b521498ca29036b0f810ec411571',
                plot_int: 2,
                start: 1654239477221,
                end: 1654804957000,
                item_type: 'land',
                sale_type: 'sale',
                pricing_model: 'auction',
                state: 'created',
                sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72538'
              },
              {
                _id: '62ab164c4d32769954701a52',
                start: 1655379831266,
                end: 1655466231266,
                item_type: 'land',
                sale_type: 'sale',
                seller: '0x8ddfab7c38d6c4f6dbc478cf4fadf973d02304d0',
                pricing_model: 'auction',
                timestamp: 1655379532197,
                plot_int: 12,
                state: 'created',
                sale_id: 'cd6cc283-c97a-4143-9514-c4923bbd9359'
              },
              {
                _id: '62a302af4d32769954701a3e',
                start: 1654850519148,
                end: 1655455319148,
                item_type: 'land',
                sale_type: 'sale',
                seller: '0xb8b7c154398db6e61c8d8e8b5b97480dc625d490',
                pricing_model: 'auction',
                timestamp: 1654850223877,
                plot_int: 12,
                state: 'created',
                sale_id: '8cdc4228-30c6-47fa-ab33-902e3b3c4553'
              },
              {
                _id: '629bb08661c541d116e2d7b2',
                timestamp: 1654239484944,
                seller: '0x5a208fae7d74b521498ca29036b0f810ec411571',
                plot_int: 1,
                start: 1654239477221,
                end: 1654325877221,
                item_type: 'land',
                sale_type: 'sale',
                pricing_model: 'auction',
                state: 'created',
                sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72537'
              },
              {
                _id: '629d1125a0c544bff1995680',
                timestamp: 1654372957000,
                seller: '0x5a208fae7d74b521498ca29036b0f810ec411571',
                plot_int: 1,
                start: 1654239477221,
                end: 1654804957000,
                item_type: 'land',
                sale_type: 'sale',
                pricing_model: 'auction',
                state: 'created',
                sale_id: 'd9d8c3b2-f3f9-4c26-88eb-332033d72531'
              }
            ]
        }
        
    },
    
    'land/plotquery': {
        12: {
                result: [
                    {
                        _id: '62ab14524d32769954701a51',
                        id: '12',
                        plot_int: 12,
                        land_status: 'forest',
                        fertility: 0.75,
                        minerals: 0,
                        owner: '0x8ddfab7c38d6c4f6dbc478cf4fadf973d02304d0',
                        timestamp: 1655379026024
                    }
                ]
            
        },
        

    },
    
    '/query': {
        0: {error: false,
            result:  [
            
                  {
                    _id: '62a38d244d32769954701a47',
                    sale_id: '8cdc4228-30c6-47fa-ab33-902e3b3c4553',
                    bid: 500,
                    address: '0xf51ecd17f361847c6f1453a3d02fca16368dc7ea',
                    timestamp: 1654885668087
                  },
                  {
                    _id: '62a363274d32769954701a44',
                    sale_id: '8cdc4228-30c6-47fa-ab33-902e3b3c4553',
                    bid: 10,
                    address: '0xf51ecd17f361847c6f1453a3d02fca16368dc7ea',
                    timestamp: 1654874919229
                  },
                  {
                    _id: '62a360bf4d32769954701a42',
                    sale_id: '8cdc4228-30c6-47fa-ab33-902e3b3c4553',
                    bid: 5,
                    address: '0x8ddfab7c38d6c4f6dbc478cf4fadf973d02304d0',
                    timestamp: 1654874303538
                  },
                  {
                    _id: '62a360d04d32769954701a43',
                    sale_id: '8cdc4228-30c6-47fa-ab33-902e3b3c4553',
                    bid: 5,
                    address: '0xf51ecd17f361847c6f1453a3d02fca16368dc7ea',
                    timestamp: 1654874320355
                  }
                ]
        }

    }
    
    
            
    
    
    
}