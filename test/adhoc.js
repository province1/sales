import * as chai from 'chai';
var should = chai.should();

import { FindLandByPlotInt, GetAllSales, GetAllCreatedAuctions, FindBidsBySaleId, FindEquipmentBySerial, FindSaleById, NetworkRequest, GetSalesByPlotInt, GetSalesBySerial} from "../utils/db_read.js";
import { test_data } from './test_request_data.js'

import { getLeadingBid } from '../utils/helpers.js'

class sim_axios{

    plot_query = async(request) => {
        return test_data[request.url][request.data.plot_int]
    }
    
    sales_all_query = async(request, idx) => {
        return test_data[request.url][idx]
    }
    
    sales_id_query = async(request) => {
        return test_data[request.url][request.data.query.sale_id];
    }
    sales_plot_query = async(request) => {
        return test_data[request.url]["plot_int"][request.data.query.plot_int];
    }
    
    bids_saleid_query = async(request, idx) => {
        return test_data[request.url][idx]
    }
    equipment_serial_query = async(request) => {
        return test_data[request.url][request.data.query.serial]
    }
    
}



describe("FindBidsBySaleId", () => {

    it("Should return without an error and a result length of zero or more.", async () => {
        const db = new sim_axios();
        const network_request = new NetworkRequest("db_service", db.bids_saleid_query.bind(db));
        const bids = await network_request.bids_query("ab9ba04f-07d4-4906-9136-d193325547fd", 0)
        
        const leading_bid = getLeadingBid(bids)
        
        /*
        const total = auctions.reduce( (x,y) => { 
            return x + y.bid }, 0
            )
        */
        //total.should.be.equal(520);
        
        console.log(234234, leading_bid)
        
        bids.length.should.be.equal(4);
    })


    // Move this to integration testing if the db_read ops is going to keep the same structure
    it("Should return without an error and a result length of zero or more.", async () => {
        const sale = await FindBidsBySaleId('ab9ba04f-07d4-4906-9136-d193325547fd');
        sale.length.should.be.equal(3)
    })

});