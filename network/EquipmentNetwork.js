import axios from 'axios'

import {
  EquipmentAlreadyOnSale,
} from "../utils/errors.js";


export class EquipmentNetworkRequest{
    
    constructor(serial, API){
        this.serial = serial;
        this.equipment_baseURL = process.env.EQUIPMENT_URL
        this.request_headers = {
            Authorization: `Basic ${process.env.EQUIPMENT_AUTH}`,
            "Content-Type": "application/json",
        }
        this.API = API;
    }
    
    error_handler(error){
        
        console.log(error)

        if(error.response.data.error_msg == 'Equipment already palaced for sale'){
            throw(EquipmentAlreadyOnSale);
        }
        
        throw("Unplanned Equipment Network Error: ", error);
        
    }
    
    async start_sale(){
        
        const data = {serial: this.serial};
        const url = "sale/start";
        const equipment_service = axios.create({baseURL: this.equipment_baseURL, headers: this.request_headers});
        return await this.API( data, equipment_service, url, this.error_handler);

    }
    
    async clear_sale(sale_id){
        const data = {serial: this.serial, sale_id};
        const url = "sale/end";
        const equipment_service = axios.create({baseURL: this.equipment_baseURL, headers: this.request_headers});
        return await this.API( data, equipment_service, url, this.error_handler)
    }
    
    async clear_lease(sale, buyer_address, days){
        const data = {senderAddress: sale.seller, lessee: buyer_address,  days, serial: this.serial };
        const url = "leasing";
        const equipment_service = axios.create({baseURL: this.equipment_baseURL, headers: this.request_headers});
        return await this.API( data, equipment_service, url, this.error_handler)
    }
    
    
}