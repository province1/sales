


export class API {
    
    async get_request( db_service, url ){
        
        let result
        
        try{
            
            result = await db_service.get(url);
            return result.data
            
            
        } catch(err) {
            
            // ToDo: Response should return an error object with atleast a message and code

            console.log("GET Request Error", err)
            console.log("GET Request Error",  err.response.data, err.response.data.error_code);
            

        }
    }

    
    
    async post_request( data, service, url, error_handler){
    
        let result
    

        try{
            
           
            result = await service.post(url, data);
            
            return result.data
            
        
            
        } catch(err) {
            
            // ToDo: Response should return an error object with atleast a message and code
            
            error_handler(err);
            
            
            
        }
        
    
    }

    
    
    
    
    
}