import {
  InsertLand,
  InsertSale,
} from "../utils/db_service.js";

import {
  FindLandByPlotInt,
  FindBidsBySaleId,
  FindSaleById,
  GetAllSales,
  GetSalesByPlotInt,
  FindEquipmentBySerial,
  GetSalesBySerial,
  GetAllCreatedAuctions
} from "../utils/db_read.js";


import {API} from '../network/Api.js'

import {EquipmentNetworkRequest} from '../network/EquipmentNetwork.js'


import { SalesSchema } from "../schemas/SalesSchema.js";
import { searchLandByAddress } from "../utils/public_data_service.js";
import { TransferFunds } from "../utils/TransferService.js";
import {
  isSaleEnded,
  saleExists,
  IsSellerOwner,
  IsFixedPrice,
  IsOnSale,
  NormalizeAddress,
  IsAllowanceEnough,
  IsIdle,
  getLeadingBid
} from "../utils/helpers.js";

import { verify_bid, VerifySignature } from "../utils/web3_tools.js";

export function Verify() {
  return async function (req, res, next) {
    try {
      const { msg, hash } = req.body;
      const jsonMessage = JSON.parse(msg);
      const { seller, address } = jsonMessage;
      delete jsonMessage.random_string;
      if(seller){
        VerifySignature(msg, hash, seller);
      }else{
        VerifySignature(msg, hash, address);
      }
      req.msg = jsonMessage;
      next();
    } catch (err) {
      next(err);
    }
  };
}

export function SaleSwitch() {
  return async function (req, res, next) {
    const { item_type } = req.msg;

    switch (item_type) {
      case "land":
        await SellLand(req, res, next);
        break;
      case "equipment":
        await SellEquipment(req, res, next);
        break;
    }
  };
}

async function SellLand(req, res, next) {
  try {
    const { plot_int, seller } = req.msg;
    const landArray = await FindLandByPlotInt(plot_int);
    IsSellerOwner(landArray, seller);
    const salesArray = await GetSalesByPlotInt({ plot_int });
    IsOnSale(salesArray);
    const validatedData = await SalesSchema.validateAsync(req.msg);
    validatedData.timestamp = Date.now();
    await InsertSale(validatedData);
    return res.status(200).json({ sale_id: validatedData.sale_id }).end();
  } catch (err) {
    next(err);
  }
}


function equipmentAPI(serial){
  const Api = new API();
  const api_post = Api.post_request.bind(Api);
  return new EquipmentNetworkRequest(serial,  api_post);
}


async function SellEquipment(req, res, next) {
  try{
    const {serial, seller} = req.msg
    //check owner of equipment
    const equipment = await FindEquipmentBySerial(serial);
    IsSellerOwner(equipment, seller);
    //check if the equipments is idle
    IsIdle(equipment);
    //check if is already on sale
    const salesArray = await GetSalesBySerial({serial})
    // This is done by the equipment service, but good to have extra redundancy
    IsOnSale(salesArray);
    //validate schema
    const validatedData = await SalesSchema.validateAsync(req.msg);
    //if is a lease, escrow lock
    if(validatedData.sale_type === 'lease'){
      //Escrow lock function
    }
    
    const network_request = equipmentAPI(serial)
    await network_request.start_sale()

    validatedData.timestamp = Date.now();

    //Insert Sale
    await InsertSale(validatedData);
    return res.status(200).json({ sale_id: validatedData.sale_id }).end();
  }catch(err){
    next(err)
  }
}

export function SaleWinner() {
  return async function (req, res, next) {
    try {
      const { sale_id } = req.body;
      const sale = await FindSaleById({ sale_id });
      saleExists(sale);
      if (sale.end < Date.now()) {
        const bidsOnSale = await FindBidsBySaleId(sale_id);
        const winningBid = bidsOnSale[0];
        return res
          .status(200)
          .json({ winner_address: winningBid.address })
          .end();
      }
    } catch (err) {
      next(err);
    }
  };
}

export function ReadSales() {
  return async function (req, res, next) {
    try {
      const response = await GetAllSales();
      return res.status(200).json(response.result).end();
    } catch (err) {
      next(err);
    }
  };
}

export function CancelSale() {
  return async function (req, res, next) {
    try {
      const { sale_id, address } = req.msg
      const landArray = await searchLandByAddress(NormalizeAddress(address));
      const sale = await FindSaleById({ sale_id });
      saleExists(sale);
      IsFixedPrice(sale);
      isSaleEnded(sale);
      const { plot_int } = sale;
      IsSellerOwner(landArray, plot_int);
      const newSale = await SalesSchema.validateAsync({
        ...sale,
        state: "cancelled",
        timestamp: Date.now(),
      });
      await InsertSale(newSale);
      res.status(200).json("Sale cancelled successfully").end();
    } catch (err) {
      next(err);
    }
  };
}


async function BuyChecks(sale, req,  address){
    saleExists(sale);
    // Verify that it has not ended
    isSaleEnded(sale);
    // Verify that the sale is fixed price
    IsFixedPrice(sale);
    // Validate the hash
    const { msg,hash } = req.body;
    await verify_bid(msg, address, req.msg, hash);
    // Verify the user has authorized a credit limit for the treasury equal to purchase price
    await IsAllowanceEnough(address, sale);

}

// export needed for unit testing
export async function ClearEquipmentSales(sale, buyer_address){
  const network_request = equipmentAPI(sale.serial);
  if(sale.sale_type == "lease"){
    const lease_days = sale.lease_length * 30.4375
    network_request.clear_lease(sale, buyer_address, sale.lease_length, lease_days)
  } else {
    await network_request.clear_sale(sale.sale_id);
  }
  
  
}

export async function ClearLandSales(sale, address){
  const { plot_int } = sale;
  const landArray = await FindLandByPlotInt(plot_int);
  const land = landArray[0];
  delete land._id;
  await InsertLand({ ...land, owner: address, timestamp: Date.now() });
  
}



export function Buy() {
  return async function (req, res, next) {
    try {

      const { sale_id, address } = req.msg;
      
      // Verify that the sale exists
      const sale = await FindSaleById({ sale_id });
      
      delete sale._id;
      
      await BuyChecks(sale, req, address);
     
      // Make the transfer to the treasury (this will be transferred to the seller on the next day)
      var test = await TransferFunds(address, sale.price);
      
      // If the transfer was OK, mark the sale as ended
      await InsertSale({
        ...sale,
        state: "ended",
        buyer: address,
        timestamp: Date.now(),
        end: Date.now(),
      });
      
      switch (sale.item_type) {
      case "land":
        await ClearLandSales(sale, address);
        break;
      case "equipment":
        await ClearEquipmentSales(sale, address);
        break;
    }

      return res.status(200).end();
    } catch (err) {
      next(err);
    }
  };
}


async function cancelled_auction(sale){
  
  await InsertSale({
          ...sale,
          state: "cancelled",
          timestamp: Date.now(),
          end: Date.now(),
  });
  
}

async function executed_sales(bidsOnSale, sale, sale_id){
  
  // **TODO:  !! DO NOT GET THE LEADING BID THIS WAY. FIX BY ADDING A NEW METHOD TO GET BOTH ADDRESS AND LEADING BID !!
  const winningBid = bidsOnSale[0];
  
  const transfer_amount = getLeadingBid(bidsOnSale);
  await TransferFunds(winningBid.address, transfer_amount);
  await InsertSale({
        ...sale,
        state: "ended",
        buyer: winningBid.address,
        timestamp: Date.now(),
        end: Date.now(),
      });

      switch (sale.item_type) {
        case "land":
          await ClearLandSales(sale, winningBid.address);
          break;
        case "equipment":
          await ClearEquipmentSales(sale, winningBid.address);
          break;
      }
}


export function EndSales(){
  return async function (req,res,next){
    //query auctions
    const sales = await GetAllCreatedAuctions();
    
    //check if they are ended
    const ByEndDate = sale => sale.end < Date.now();
    
    const endedSales = sales.filter(ByEndDate);
    
    Promise.all( endedSales.map( async (sale) => {
      
      delete sale._id;
      const {sale_id, plot_int} = sale
      
      const bidsOnSale = await FindBidsBySaleId(sale_id);
      
      // Auction ends with no bids
      if(bidsOnSale.length == 0){
        await cancelled_auction(sale)
      } else {
        await executed_sales(bidsOnSale, sale, sale_id)
      }
    }))
    


    return res.status(200).end();
  }
}
