import {

  InsertBid,
} from "../utils/db_service.js";

import {
  FindBidsBySaleId,
  FindSaleById,
} from "../utils/db_read.js";


import {
  getBidsByUserAddress,
  getLeadingBid,
  hideBidData,
  isAuction,
  isEmpty,
  isHigherThanLeadingBid,
  isSaleEnded,
  isSaleStarted,
  isUsersHighestBid,
  saleExists,
} from "../utils/helpers.js";
import { verify_bid } from "../utils/web3_tools.js";
import { BidSchema } from "../schemas/BidSchema.js";

export function CreateBid() {
  return async function (req, res, next) {
    try {
      const { msg, hash } = req.body;
      const newBid = JSON.parse(msg);
      const { sale_id, address } = newBid;
      delete newBid.random_string;
      console.log("New Bid submitted to sale: ", sale_id);
      const sale = await FindSaleById({ sale_id });
      // console.log("sale found", sale);
      saleExists(sale);
      isAuction(sale);
      isSaleEnded(sale);
      isSaleStarted(sale);
      //read the bids with the same sale id
      const bidsOnSale = await FindBidsBySaleId(sale_id);
      if (isEmpty(bidsOnSale)) {
        //just save the first bid
        await verify_bid(msg, address, newBid, hash);
        const validatedBid = await BidSchema.validateAsync(newBid);
        await InsertBid({ ...validatedBid, timestamp: Date.now() });
      } else {
        //compare to users current highest
        const userBids = getBidsByUserAddress(bidsOnSale, address);
        isUsersHighestBid(userBids, newBid);
        //check if its higher than the leading bid
        const leadingBid = getLeadingBid(bidsOnSale);
        // console.log("Current bid Array: ", bidsOnSale)
        // console.log("Current leading bid: ", leadingBid)
        isHigherThanLeadingBid(leadingBid, newBid);
        await verify_bid(msg, address, newBid, hash);
        const validatedBid = await BidSchema.validateAsync(newBid);
        await InsertBid({ ...validatedBid, timestamp: Date.now() });
      }
      return res.status(200).end();
    } catch (err) {
      next(err);
    }
  };
}

export function ReadBids() {
  return async function (req, res, next) {
    try {
      const { sale_id } = req.body;
      const sale = await FindSaleById({ sale_id });
      saleExists(sale);
      isAuction(sale);
      const bidsOnSale = await FindBidsBySaleId(sale_id);
      const leadingBid = getLeadingBid(bidsOnSale);

      // Only the max bid(s) needs to be hidden. The rest can be public.
      const bidsOnSaleWithHiddenData = hideBidData(bidsOnSale, leadingBid);
      return res.status(200).json(bidsOnSaleWithHiddenData).end();
    } catch (err) {
      next(err);
    }
  };
}

export function GetLeadingBid() {
  return async function (req, res, next) {
    try {
      const { sale_id } = req.body;
      const sale = await FindSaleById({ sale_id });
      saleExists(sale);
      isAuction(sale);
      const bidsOnSale = await FindBidsBySaleId(sale_id);
      const leading_bid = getLeadingBid(bidsOnSale);
      return res.status(200).json({ leading_bid }).end();
    } catch (err) {
      next(err);
    }
  };
}
