import Joi from "joi";

export const RequestSchema = Joi.object({
  msg: Joi.string().required(),
  hash: Joi.string().required(),
})
