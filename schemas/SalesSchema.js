import Joi from "joi";
import { v4 as uuidv4 } from "uuid";

//Sales Schema
export const SalesSchema = Joi.object({
  start: Joi.date().timestamp("javascript").less(Joi.ref("end")).required(),
  end: Joi.date().timestamp("javascript").required(),
  item_type: Joi.string().valid("land", "equipment").required(),
  plot_int: Joi.when("item_type", {
    is: "land",
    then: Joi.number().integer().required(),
    otherwise: Joi.forbidden(),
  }),
  serial: Joi.when("item_type", {
    is: "equipment",
    then: Joi.number().required(),
    otherwise: Joi.forbidden(),
  }),
  sale_type: Joi.string().valid("sale", "lease").required(),
  pricing_model: Joi.string().valid("auction", "fixed-price").required(),
  sale_id: Joi.string(),
  lease_length: Joi.when("sale_type", {
    is: "lease",
    then: Joi.number().integer().min(6).max(24).required(),
    otherwise: Joi.forbidden(),
  }),
  price: Joi.when("pricing_model", {
    is: "fixed-price",
    then: Joi.number().integer().required(),
    otherwise: Joi.forbidden(),
  }),
  state: Joi.string().valid("created","cancelled").default("created"),
  buyer: Joi.when("state", {
    is: "ended",
    then: Joi.string().pattern(/^0x[a-fA-F0-9]{40}$/),
    otherwise: Joi.forbidden(),
  }),
  seller: Joi.string().pattern(/^0x[a-fA-F0-9]{40}$/),
  timestamp: Joi.date().timestamp("javascript").default(Date.now()),
}).custom((obj, helpers) => {
  const { start, end, sale_id, seller } = obj;

  /*
  Adds an additional allowance of 30 minutes to account for block processing times between when user starts process and when the allowance tx completes allowing him to continue the
  process without being over the 7 day start limit.
  */
  const processing_time_allowance = 30 * 60 * 1000
  
  const startPlusSevenDays =  new Date().getTime() + 7 * 24 * 60 * 60 * 1000 + processing_time_allowance

  const nowMinusFiveMinutes = new Date().getTime() - 5 * 60 * 1000;

  const startPlusTwentyFourHours = new Date(
    start.getTime() + 24 * 60 * 60 * 1000
  );

  const startPlusThirtyDays = new Date(
    start.getTime() + 30 * 24 * 60 * 60 * 1000
  );

  if(start < nowMinusFiveMinutes){
    return helpers.error("start can not be in the past");
  }

  if (start > startPlusSevenDays) {
    return helpers.error("start must be within 7 days from now");
  }
  if (end < startPlusTwentyFourHours || end > startPlusThirtyDays) {
    return helpers.error(
      "end must be between start + 24hrs and start + 30 days"
    );
  }
  return {
    ...obj,
    start: start.getTime(),
    end: end.getTime(),
    sale_id:sale_id || uuidv4(),
    seller:seller.toLowerCase(),
  };
});
