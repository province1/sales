import Joi from "joi";

export const BidSchema = Joi.object({
  bid: Joi.number().integer().min(1).required(),
  address: Joi.string()
    .pattern(/^0x[a-fA-F0-9]{40}$/)
    .required(),
  timestamp: Joi.date().timestamp("javascript"),
  sale_id: Joi.string().required(),
}).custom((obj, helpers) => {
  const { date, address } = obj;
  return { ...obj, address:address.toLowerCase() };
});
