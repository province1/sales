import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import dotenv from "dotenv";
import morgan from "morgan";
import { Validate } from "./utils/validator.js";
import {
  RequestSchema
} from "./schemas/index.js";
import { errorHandler } from "./utils/errorHandler.js";
import {
  SaleWinner,
  ReadSales,
  CancelSale,
  Buy,
  Verify,
  SaleSwitch,
  EndSales
} from "./services/SaleServices.js";
import {CreateBid, ReadBids, GetLeadingBid} from './services/BidServices.js'

dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const helmet_config = { crossOriginResourcePolicy: { policy: "same-site" } };

app.use(helmet(helmet_config));
app.use(morgan("dev", { skip: (req, res) => process.env.NODE_ENV === "test" }));

//Sales Services
app.post("/api/sales/create", Validate(RequestSchema),Verify(),SaleSwitch());
app.post("/api/sales/winner", SaleWinner());
app.get("/api/sales/read", ReadSales());
app.post("/api/sales/cancel", Validate(RequestSchema), Verify(),CancelSale());
app.post("/api/sales/buy",Validate(RequestSchema),Verify(), Buy());
app.get("/api/sales/end",EndSales())
//Bids Services
app.post("/api/bids/create", Validate(RequestSchema), CreateBid());
app.post("/api/bids/read", ReadBids());
app.post("/api/bids/leading",GetLeadingBid())

app.use(errorHandler);

const port = process.env.NODE_ENV === "test" ? 5000 : process.env.PORT;

app.listen(port, () => {
  console.log(`${process.env.NAME} listening at http://localhost:${port}`);
});

export default process.env.NODE_ENV === "test" ? app : null;
