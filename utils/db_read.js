import axios from "axios";
import dotenv from "dotenv";

import { database_service } from "db_service_adapter";

dotenv.config();
const AUTH = process.env.AUTH;
const BASE_URL = process.env.BASE_URL;

const urls = {
  current_state: "/current_state",
  current_state_many: "/current_state_many",
  current_state_filter: "current_state_filter"
};


const baseURLs = {
    
    public_data: process.env.PUBLIC_DATA_URL,
    db_service: process.env.BASE_URL
}

const get_data = async (request, test=null) => {
  
  try {
    const response = await axios(request)
    if (response.data.error) {
        throw new Error(response.data.error_msg);
    }
    return response.data;
            
  } catch (err) {
    throw err;
  }
}

const get_data2 = async (request, test=null) => {
  
  try {
    const response = await axios(request)
    if (response.data.error) {
        throw new Error(response.data.error_msg);
    }
    return response;
            
  } catch (err) {
    throw err;
  }

}

export class NetworkRequest {
    
    constructor(type, db){
        
        this.baseUrl = baseURLs[type]
        this.db = db;
        
    }
    
    request_data_post = () => {
      return {
            method: "POST",
            baseURL: this.baseUrl,
            headers: {
                Authorization: `Basic ${AUTH}`,
                "Content-Type": "application/json",
            },
        }
    }
    
    request_data_get = () => {
      return {
            method: "GET",
            baseURL: this.baseUrl,
            headers: {
                Authorization: `Basic ${AUTH}`,
                "Content-Type": "application/json",
            },
        }
    }
    

    plot_query = async (plot_int) => {
  
      var plot_request = this.request_data_post();
      plot_request.url = "land/plotquery"
      plot_request.data = {plot_int}
      const result = await this.db(plot_request);
      return result.result
            
    }
    
    sales_query = async(test=null) => {
      var sales_request = this.request_data_get();
      sales_request.url = "sales"
      return await this.db(sales_request, test);

    }
    
    bids_query = async(sale_id, test=null) => {
      var bids_request = this.request_data_post();
      bids_request.url= "/query",
      bids_request.data = { query: { sale_id }, collection: "bids", sort: { bid: -1 } }
      const bids =  await this.db(bids_request, test);
      return bids.result

    }
    
    auction_query = async (test=null) => {
      var auction_request = this.request_data_post();
      auction_request.url = urls['current_state_filter']
      auction_request.data = {filter: { "state": "created","pricing_model":"auction" },collection: "sales",grouper: "sale_id",}
      const auctions =  await this.db(auction_request, test);
      return auctions.result;

    }
    
    equipment_serial_query = async (serial, test=null) => {
      
      var equipment_request = this.request_data_post();
      equipment_request.url = urls['current_state']
      equipment_request.data =  { query: { serial: serial }, collection: "equipment" }
      const result =  await this.db(equipment_request, test);
      return result.result
      
    }
    
    find_factory = async (query, test=null) => {
      

        var request = this.request_data_post();
        request.url = urls.current_state;
        request.data = { query: query, collection: 'sales' }
        const results = await this.db(request, test);
        if (results.result.length === 1) {
          return results.result[0];
        }
        
        return results.result;

    }
    
    find_factory2 = async (query, test=null) => {
      

        var request = this.request_data_post();
        request.url = urls.current_state;
        request.data = { query: query, collection: 'sales' }
        const results = await this.db(request, test);
        if (results.data.error) {
          throw new Error(results.data.error_msg);
        }

      return results.data.result;

    }

}

export async function FindSaleById(query) {
    const network_request = new NetworkRequest("db_service", get_data);
    return await network_request.find_factory(query);
}


export async function GetSalesByPlotInt(query) {
    const network_request = new NetworkRequest("db_service", get_data2);
    return await network_request.find_factory2(query);
}

// ToDo: Unit Testing
export async function GetSalesBySerial(query) {
    const network_request = new NetworkRequest("db_service", get_data2);
    return await network_request.find_factory2(query);
}

export async function FindLandByPlotInt(plot) {
    const network_request = new NetworkRequest("public_data", get_data);
    return await network_request.plot_query(plot);
}

export async function FindBidsBySaleId(sale_id) {
  const network_request = new NetworkRequest("db_service", get_data);
  return await network_request.bids_query(sale_id);
}

export async function GetAllSales() {
  const network_request = new NetworkRequest("public_data", get_data);
  return await network_request.sales_query2();
}

export async function GetAllCreatedAuctions() {
    const network_request = new NetworkRequest("db_service", get_data);
    return await network_request.auction_query();
}

export async function FindEquipmentBySerial(serial) {
  const network_request = new NetworkRequest("db_service", get_data);
  return await network_request.equipment_serial_query(serial);
}
