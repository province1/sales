import {
  SaleHasEndedError,
  SaleHasStartedError,
  SaleIsNotAuctionError,
  SaleNotFoundError,
  BidIsLowerError,
  BidIsLowerThanLeadingError,
  SellerIsNotOwnerError,
  SaleIsNotFixedPriceError,
  ItemAlreadyOnSale,
  AllowanceError,
  EquipmentIsNotIdleError,
} from "./errors.js";
import "../typedefs/types.js";

import { get_mpl_allowance } from "./web3_tools.js";

/**
 * @function IsSellerOwner
 * @param {Sale[]} landArray
 * @param {number} plot_int
 * @param {string} seller
 * @throws {Error} Seller and Owner are not the same
 * @returns {void}
 */
export const IsSellerOwner = (landArray, seller) => {
  if (landArray[0].owner !== seller) {
    throw SellerIsNotOwnerError;
  }
};

export const saleExists = (sale) => {
  if (!sale) {
    throw SaleNotFoundError;
  }
};

export const isAuction = (sale) => {
  if (sale.pricing_model !== "auction") {
    throw SaleIsNotAuctionError;
  }
};

export const IsFixedPrice = (sale) => {
  if (sale.pricing_model !== "fixed-price") {
    throw SaleIsNotFixedPriceError;
  }
};

export const isSaleEnded = (sale) => {
  if (sale.end < Date.now()) {
    throw SaleHasEndedError;
  }
};

export const isSaleStarted = (sale) => {
  if (sale.start > Date.now()) {
    throw SaleHasStartedError;
  }
};

/**
 * @function isEmpty
 * @param {Array} arr
 * @returns {boolean}
 */
export const isEmpty = (arr) => arr.length === 0;

export const getBidsByUserAddress = (bidsOnSale, userAddress) => {
  return bidsOnSale.filter((bid) => bid.address === userAddress);
};

/**
 * @function isUsersHighestBid
 * @param {Bid[]} userBids
 * @param {Bid} newBid
 * @returns {void}
 * @throws {Error} Bid is lower than your current bid
 */
export const isUsersHighestBid = (userBids, newBid) => {
  if (userBids.some((bid) => bid.bid >= newBid.bid)) {
    throw BidIsLowerError;
  }
};

/**
 * @function getLeadingBid
 * @param {Bid[]} bidsOnSale
 * @returns {number}
 */
export function getLeadingBid(bidsOnSale) {
  if (bidsOnSale.length > 1) {
    const orderedBids = bidsOnSale.sort((a, b) => (a.bid > b.bid ? -1 : 1));

    // Corner case where 2 or more people place the same bids
    const leadingBid =
      orderedBids[0].bid == orderedBids[1].bid
        ? orderedBids[0].bid
        : orderedBids[1].bid + 1;

    return leadingBid;
  }

  return bidsOnSale.length;
}

/**
 * @function isHigherThanLeadingBid
 * @param {number} leadingBid
 * @param {Bid} newBid
 * @returns {void}
 * @throws {Error} Bid is lower than the leading bid
 */
export const isHigherThanLeadingBid = (leadingBid, newBid) => {
  if (newBid.bid < leadingBid) {
    throw BidIsLowerThanLeadingError;
  }
};

/**
 * @function hideBidData
 * @param {Bid[]} bidsOnSale
 * @returns {Object}
 */
export const hideBidData = (bidsOnSale, leadingBid) =>
  bidsOnSale.map((bid_doc) => {
    var { _id, address, date, bid } = bid_doc;

    bid = bid >= leadingBid ? leadingBid : bid;

    return {
      _id,
      address,
      bid,
      date,
    };
  });

export const IsOnSale = (salesArray) => {
  salesArray.map((sale) => {
    if (sale.end > Date.now() && sale.state !== "cancelled") {
      throw ItemAlreadyOnSale;
    }
  });
};

export const IsIdle = (equipment) => {
  if(equipment[0].status !== "idle"){
    throw EquipmentIsNotIdleError;
  }
}

export const NormalizeAddress = (address) => address.toLowerCase();

export const IsAllowanceEnough = async (address, sale) => {
  const allowance = await get_mpl_allowance(address);

  if (allowance < sale.price) {
    throw AllowanceError;
  }
};
