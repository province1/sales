import axios from "axios";
import dotenv from "dotenv";

import { database_service } from "db_service_adapter";

dotenv.config();
const AUTH = process.env.AUTH;
const BASE_URL = process.env.BASE_URL;

const urls = {
  current_state: "/current_state",
};


export async function InsertSale(sale) {
  try {
    const response = await axios({
      method: "POST",
      url: "/insert_single",
      baseURL: BASE_URL,
      data: { entry_doc: sale, collection: "sales" },
      headers: {
        Authorization: `Basic ${AUTH}`,
        "Content-Type": "application/json",
      },
    });
    
    if (response.data.error) {
      throw new Error(response.data.error_msg);
    }
    return response;
  } catch (err) {
    throw err;
  }
}

export async function InsertBid(bid) {
  try {
    const response = await axios({
      method: "POST",
      url: "/insert_single",
      baseURL: BASE_URL,
      data: { entry_doc: bid, collection: "bids" },
      headers: {
        Authorization: `Basic ${AUTH}`,
        "Content-Type": "application/json",
      },
    });
    if (response.data.error) {
      throw new Error(response.data.error_msg);
    }
    return response;
  } catch (err) {
    throw err;
  }
}

export async function InsertLand(land) {
  try {
    const response = await axios({
      method: "POST",
      url: "/insert_single",
      baseURL: BASE_URL,
      data: { entry_doc: land, collection: "land_data" },
      headers: {
        Authorization: `Basic ${AUTH}`,
        "Content-Type": "application/json",
      },
    });
    if (response.data.error) {
      throw new Error(response.data.error_msg);
    }
    return response;
  } catch (err) {
    throw err;
  }
}

