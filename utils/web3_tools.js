import Moralis from "moralis/node.js";
import dotenv from "dotenv";
dotenv.config();
import { readFileSync } from "fs";
import {
  BidIsLowerThanBalance,
  AllowanceError,
  SignatureValidationError,
} from "./errors.js";

const ethers = Moralis.web3Library;

const file = readFileSync("./MapleContract.json", "utf-8");
const MapleContract = JSON.parse(file);

const node_url = process.env.NODE_URL;


const Contract = ()=>{
  
  const provider = new ethers.providers.JsonRpcProvider(node_url);
  
  const contract = new ethers.Contract(process.env.MAPLE_ADDRESS, MapleContract.abi, provider);
  
  return contract
  

}







const treasury_address = process.env.TREASURY_ADDRESS;

async function check_tmp_balance(address) {
  // const provider = new ethers.providers.JsonRpcProvider(node_url);

  // const contract = new ethers.Contract(MapleContract.contract_address, MapleContract.abi, provider)
  
  const contract = Contract();
  
  const hexBalance = await contract.balanceOf(address);
  const balance = parseInt(ethers.utils.formatEther(hexBalance));

  return balance;
}

export async function get_mpl_allowance(address){

    const contract = Contract();
    const hexAllowance = await contract.allowance(address, treasury_address)
    
    //console.log("tmp allowance: ", parseInt(ethers.utils.formatEther(allowance)))
    const allowance = parseInt(ethers.utils.formatEther(hexAllowance))
    return allowance
                                            
}

export function VerifySignature(msg, hash, address) {
  try {
    console.log("Address", address)
    console.log("msg", msg)
    console.log(typeof msg)
    console.log("hash", hash)
    const signatureAddress = ethers.utils.verifyMessage(msg, hash);
    console.log("Signature Address",signatureAddress);
    if (signatureAddress.toLowerCase() !== address.toLowerCase()) {
      console.log("Signature Validation Error", signatureAddress.toLowerCase(), address.toLowerCase())
      throw SignatureValidationError;
    }
  } catch (err) {
    console.log("signature validation error (unhandled", err)
    throw SignatureValidationError;
  }
}

export async function verify_bid(message_str, address, message_json, hash) {
  VerifySignature(message_str, hash, address);
  const balance = await check_tmp_balance(address);

    if( balance < message_json.bid || balance < message_json.price){
        throw BidIsLowerThanBalance
    }
    const allowance = await get_mpl_allowance(address)
    
    if( allowance < message_json.bid || balance < message_json.price){
        throw AllowanceError
    }
    
    return 
    
}
