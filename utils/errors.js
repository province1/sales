export const EquipmentAlreadyOnSale = new Error("Equipment already palaced for sale");
EquipmentAlreadyOnSale.error_code = 15
EquipmentAlreadyOnSale.name = "EquipmentAlreadyOnSale";

export const EquipmentIsNotIdleError = new Error("Equipment is not idle");
EquipmentIsNotIdleError.error_code = 14
EquipmentIsNotIdleError.name = "EquipmentIsNotIdleError"

export const TransferFundsError = new Error("Error transferring funds");
TransferFundsError.error_code = 13
TransferFundsError.name = "TransferFundsError"

export const SaleNotFoundError = new Error("Sale ID not found");
SaleNotFoundError.error_code = 12
SaleNotFoundError.name = "SaleNotFoundError";

export const SaleIsNotAuctionError = new Error("Sale is not an auction");
SaleIsNotAuctionError.error_code = 11
SaleIsNotAuctionError.name = "SaleIsNotAuctionError";

export const SaleIsNotFixedPriceError = new Error("Sale is not fixed price");
SaleIsNotFixedPriceError.error_code=10
SaleIsNotFixedPriceError.name = "SaleIsNotFixedPriceError";

export const SaleHasEndedError = new Error("Sale has ended");
SaleHasEndedError.error_code = 9
SaleHasEndedError.name = "SaleHasEndedError";

export const SaleHasStartedError = new Error("Sale has not started yet");
SaleHasStartedError.error_code = 8
SaleHasStartedError.name = "SaleHasStartedError";

export const BidIsLowerError = new Error("Bid is lower than your current bid");
BidIsLowerError.error_code = 7
BidIsLowerError.name = "BidIsLowerError";

export const BidIsLowerThanLeadingError = new Error("Bid is lower than the leading bid");
BidIsLowerThanLeadingError.error_code = 6
BidIsLowerThanLeadingError.name = "BidIsLowerThanLeadingError";

export const BidIsLowerThanBalance= new Error("Bid is lower than the address balance");
BidIsLowerThanBalance.error_code = 5
BidIsLowerThanBalance.name = "BidIsLowerThanBalance";

export const AllowanceError = new Error("Allowance is not enough");
AllowanceError.error_code = 4
AllowanceError.name = "AllowanceError";

export const SellerIsNotOwnerError = new Error("Seller and Owner are not the same");
SellerIsNotOwnerError.error_code = 3
SellerIsNotOwnerError.name = "SellerIsNotOwnerError";

export const SignatureValidationError = new Error("Signature is not valid");
SignatureValidationError.error_code = 2
SignatureValidationError.name = "SignatureValidationError";

export const ItemAlreadyOnSale = new Error("Item is already on sale");
ItemAlreadyOnSale.error_code = 1
ItemAlreadyOnSale.name = "ItemAlreadyOnSale";
