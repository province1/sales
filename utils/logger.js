import winston from "winston";

const logConfiguration =  {
    'transports': [
        new winston.transports.Console({
            silent: process.env.NODE_ENV === "test"
        })
    ]
};

const logger = winston.createLogger(logConfiguration);

export default logger