export function Validate(validator) {
  return async function (req, res, next) {
    try {
      const validated = await validator.validateAsync(req.body);
      req.body = validated
      next();
    } catch (err) {
      next(err);
    }
  };
}