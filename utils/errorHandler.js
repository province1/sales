import logger  from "./logger.js";

export const errorHandler = (err, req, res, next) => {

  const errorWithCode = {...err,message:err.message}

  if (err.name === "ValidationError") {
    logger.error(err.message)
    res.status(400).json(err.message).send();
  }
  else if (err.name === "SaleNotFoundError") {
    logger.info(req.body.sale_id)
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if (err.name === "SaleIsNotAuctionError") {
    logger.info(req.body.sale_id)
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if (err.name === "SaleHasEndedError") {
    logger.info(req.body.sale_id)
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if (err.name === "SaleHasStartedError") {
    logger.info(req.body.sale_id)
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if (err.name === "BidIsLowerError") {
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  } 
  else if(err.name === "BidIsLowerThanLeadingError"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "BidIsLowerThanBalance"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "AllowanceError"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "SellerIsNotOwnerError"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "SignatureValidationError" ){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if (err.name ==="SyntaxError"){
    logger.error(err.message)
    res.status(400).json(err.message).send()
  }
  else if(err.name === "SaleIsNotFixedPriceError"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "ItemAlreadyOnSale"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "TransferFundsError"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "EquipmentIsNotIdleError"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else if(err.name === "EquipmentAlreadyOnSale"){
    logger.error(err.message)
    res.status(400).json(errorWithCode).send()
  }
  else {
    logger.info(req.body)
    logger.error(err)
    console.log(err.stack)
    console.trace()
    res.status(500).json("Internal server error").send();
  }
};