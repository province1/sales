import axios from "axios";
import { TransferFundsError } from "./errors.js";

const AUTH = process.env.TRANSFER_AUTH;
const BASE_URL = process.env.TRANSFER_URI;

export async function TransferFunds(sender, amountInt) {
  try {
    const amount = amountInt.toString();
    const response = await axios.post(
      BASE_URL,
      { sender, amount },
      { headers: { Authorization: `Basic ${AUTH}` } }
    );
    if(response.data.error === true){
        throw TransferFundsError
    }
  } catch (error) {
    throw TransferFundsError
  }
}
