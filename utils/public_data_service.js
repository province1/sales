import axios from "axios";
import qs from 'qs'

export async function searchLandByAddress(address) {
  try {
    const data = qs.stringify({
      'query_type':'owner',
      'query_param':address
    })
    const response = await axios({
      method: "POST",
      url: "/land_data",
      baseURL: process.env.PUBLIC_DATA_URI,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data
    });
    if (response.data.error) {
      throw new Error(response.data.error_msg);
    }
    return response.data;
  } catch (err) {
    throw err;
  }
}