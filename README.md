# sales-service

# Endpoints

Now all creation endpoints and the cancellation endpoints will only receive a message and a hash.
The message must contain documented data. Anything else will fail.
This way we can make sure that the user who signed the message is the on

# POST `/api/sales/create`

This endpoint will create a new sale. All the details about data validity are here:
[Sales Documentation](https://docs.province.gg/technical-documentation/sales)


Body: json

msg MUST contain:
`start, end, item_type, plot_int, sale_type, pricing_model, seller, random_string`


Data example:
```
{
    "msg": "{\n    \"start\": 1650277079304,\n    \"end\": 1650363479304,\n    \"item_type\": \"land\",\n    \"plot_int\": 55,\n    \"sale_type\": \"sale\",\n    \"pricing_model\": \"auction\",\n    \"seller\": \"0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b\"\n}",
    "hash": "0x7ce87beac50535f844df318601adf8f88d9f94d8c40035cc578e8d5c7b9167982d60db0420f2f434eae85098a1025268b3fc268398257aa0db86628ae2192ec91b"
}
```
Response:

```
200 OK
{
    "sale_id": "1eedc8be-242b-48a7-b249-feee3efe89b1"
}
```

Errors returned by the endpoint are pretty self explanatory.
For example:
```
400 Err

{
    "error_code": 3,
    "name": "SellerIsNotOwnerError",
    "message": "Seller and Owner are not the same"
}
```
---
# POST `/api/sales/winner`

This endpoint will return the highest bid address(winner) for a sale_id that has already ended
[Sales Documentation](https://docs.province.gg/technical-documentation/sales)


Body: json

Data example:
```
{
    "sale_id":"e7e5bbcb-5278-4fb5-adc0-ecadfb7efbab"
}
```
Response:

```
200 OK
{winner_address:"0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b"}
```

Errors returned by the endpoint are pretty self explanatory.
For example:
```
400 Err

{
    "error_code": 3,
    "name": "SellerIsNotOwnerError",
    "message": "Seller and Owner are not the same"
}
```
---
# GET `/api/sales/read`

This endpoint will return all sales.
[Sales Documentation](https://docs.province.gg/technical-documentation/sales)

Response:

```
200 OK

[]
```

Errors returned by the endpoint are pretty self explanatory.
For example:
```
400 Err

{
    "error_code": 3,
    "name": "SellerIsNotOwnerError",
    "message": "Seller and Owner are not the same"
}
```
---
# POST `/api/bids/create`

This endpoint will create a new bid for a specific sale. All the details about data validity are here:
[Sales Documentation](https://docs.province.gg/technical-documentation/sales)


Body: json

msg MUST contain: `bid, address, sale_id, random_string`

Data example:
```
{
    "msg":"{\"bid\":\"5\",\"address\":\"0x12231\",\"sale_id\":\"1234\"}", <=== Message in string
    "hash":"this is a hash", <==== hash of signed message
}
```
Response:

```
200 OK
```

Errors returned by the endpoint are pretty self explanatory.
For example:
```
400 Err

{
    "error_code": 3,
    "name": "SellerIsNotOwnerError",
    "message": "Seller and Owner are not the same"
}
```
---

# POST `/api/bids/read`

This endpoint will return all bids for the sale id in descending order (highest to lowest amount)
[Sales Documentation](https://docs.province.gg/technical-documentation/sales)


Body: json

Data example:
```
{
    "sale_id":"e7e5bbcb-5278-4fb5-adc0-ecadfb7efbab"
}
```
Response:

```
200 OK
[
    {
        _id:"123456xcas",
        address:"0x12323jskdj",
        date:123123123
    },
    {
        _id:"123456xcas",
        address:"0x12323jskdj",
        date:123123123
    },
    {
        _id:"123456xcas",
        address:"0x12323jskdj",
        date:123123123
    },
]
```

Errors returned by the endpoint are pretty self explanatory.
For example:
```
400 Err

{
    "error_code": 3,
    "name": "SellerIsNotOwnerError",
    "message": "Seller and Owner are not the same"
}

```


# ERROR CODES

* 1 => Land already on sale
* 2 => Signature is not valid
* 3 => The seller is not the owner
* 4 => The allowance set up in the wallet is not enough
* 5 => The bid being submitted is lower than the account balance
* 6 => The bid being submitted is lower than the leading bid
* 7 => The bid being submitted is lower than another bid from the same user
* 8 => The sale has not started
* 9 => The sale has already finished
* 10 => Sale is not of type fixed price (trying to cancel)
* 11 => Sale is not of type auction (trying to bid)
* 12 => The sale id does not exist
* 13 => Transfer funds error
* 14 => Equipment is not idle. Equipment must be idle if you want to sell it.
