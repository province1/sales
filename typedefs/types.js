/**
 * @typedef Bid
 * @prop {number} bid
 * @prop {string} _id
 * @prop {string} address
 * @prop {number} date
 */

/**
 * @typedef Sale
 * @prop {string} sale_id 
 * @prop {number} plot_int
 */

/**
 * @typedef Error
 * @prop {string} name
 * @prop {string} message
 */
